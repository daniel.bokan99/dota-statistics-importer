package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"

	"github.com/gorilla/mux"

	"strings"
	"syscall"
	"time"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

type Match struct {
	Match_id      int    `json:"match_id"`
	Match_seq_num int    `json:"match_seq_num"`
	Radiant_win   bool   `json:"radiant_win"`
	Start_time    int    `json:"start_time"`
	Duration      int    `json:"duration"`
	Radiant_team  string `json:"radiant_team"`
	Dire_team     string `json:"dire_team"`
}

type Metadata struct {
	Match_Count   int    `json:match_Count`
	Min_startTime int    `json:min_startTime`
	Max_startTime int    `json:max_startTime`
	Min_duration  int    `json:min_duration`
	Max_duration  int    `json:max_duration`
	Min_insertTS  string `json:min_insertTS`
	Max_insertTS  string `json:max_insertTS`
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "bazepodataka"
	dbname   = "dota"
)

func main() {
	/*
		myServer := &http.Server{
			Addr:         "",
			Handler:      handler(),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		}*/
	r := mux.NewRouter()
	r.HandleFunc("/metadata", metadataHandler)
	r.HandleFunc("/matchdata", matchdataHandler)

	go func() {
		fmt.Println("Server started")
		log.Fatal(http.ListenAndServe(":80", r))
	}()

	pg_con := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", pg_con)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	u := uuid.New()
	CloseHandler(u.String(), db)
	logStatement := `INSERT INTO logs
				("instance_id", "startTime")
				VALUES ($1, $2);`
	sqlLog, err := db.Query(logStatement, u.String(), time.Now().UTC())
	if err != nil {
		log.Fatal(err)
	}
	sqlLog.Close()
	/*
		tableCreate := `CREATE TABLE matches (
												Match_id varchar(255) UNIQUE,
												Match_seq_num varchar(255),
												Radiant_win varchar(10),
												Start_time INT,
												Duration INT,
												Radiant_team varchar(255),
												Dire_team varchar(255),
												insertTS TIMESTAMP,
												updateTS TIMESTAMP,
												updateCount INT
												);`

		cr, err := db.Query(tableCreate)
		defer cr.Close()
		if err != nil {
			log.Fatal(err)
		}*/

	myClient := &http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Timeout:       0,
	}

	req, err := http.NewRequest(http.MethodGet,
		"https://api.opendota.com/api/publicMatches",
		nil)
	data := req.URL.Query()
	data.Add("mmr_ascending", "1")
	req.URL.RawQuery = data.Encode()

	if err != nil {
		log.Fatal(err)
	}

	var m []Match
	for {
		resp, err := myClient.Do(req)

		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Status:", resp.Status)
		body, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			log.Fatal(err)
		}

		err = json.Unmarshal(body, &m)

		if err != nil {
			log.Fatal(err)
		}

		for i := range m {
			insert := `INSERT INTO matches (Match_id, 
						Match_seq_num, 
						Radiant_win, 
						Start_time, 
						Duration, 
						Radiant_team, 
						Dire_team,
						insertTS,
						updateTS,
						updateCount)
						VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
						ON CONFLICT (Match_id)
						DO UPDATE SET UpdateTS = $11, updateCount = matches.updateCount + 1;`
			ins, err := db.Query(insert, m[i].Match_id, m[i].Match_seq_num, m[i].Radiant_win,
				m[i].Start_time, m[i].Duration, m[i].Radiant_team, m[i].Dire_team, time.Now().UTC(), time.Now().UTC(), 0, time.Now().UTC())
			if err != nil {
				log.Fatal(err)
			}
			ins.Close()
		}
		resp.Body.Close()
		time.Sleep(30 * time.Minute)
	}
}

func CloseHandler(unid string, dbp *sql.DB) {
	sigs := make(chan os.Signal)
	signal.Notify(sigs, syscall.SIGINT)

	go func(instanceID string, dbpointer *sql.DB) {
		<-sigs
		fmt.Println("SIGINT received")
		//query
		dbpointer.QueryRow(`UPDATE logs SET "endTime" = $1 WHERE instance_id = $2`, time.Now().UTC(), instanceID)
		os.Exit(0)
	}(unid, dbp)
}

func metadataHandler(w http.ResponseWriter, req *http.Request) {
	pg_con := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	datab, err := sql.Open("postgres", pg_con)
	if err != nil {
		log.Fatal(err)
	}
	defer datab.Close()
	var met Metadata
	metadataStatement := `SELECT COUNT(*), MIN(start_Time), MAX(start_Time), 
				MIN(duration), MAX(duration), 
				MIN(insertTS), MAX(insertTS)
				FROM matches`
	datab.QueryRow(metadataStatement).Scan(&met.Match_Count, &met.Min_startTime, &met.Max_startTime,
		&met.Min_duration, &met.Max_duration,
		&met.Min_insertTS, &met.Max_insertTS)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Println(met)
	js, err := json.Marshal(met)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(js)
}

func matchdataHandler(w http.ResponseWriter, req *http.Request) {
	pg_con := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", pg_con)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//fmt.Println(req.URL.Query()["startTime"])
	//fmt.Println(req.URL.Query()["endTime"])

	start_ar := req.URL.Query()["startTime"]
	start_str := strings.Join(start_ar, " ")
	start, err := strconv.Atoi(start_str)
	if err != nil {
		log.Fatal(err)
	}

	end_ar := req.URL.Query()["endTime"]
	end_str := strings.Join(end_ar, " ")
	fmt.Printf(end_str)
	end, err := strconv.Atoi(end_str)
	if err != nil {
		log.Fatal(err)
	}
	var matchdata []Match
	matchStatement := `SELECT Match_id, Match_seq_num, Radiant_win,
					Start_time, Duration, Radiant_team, Dire_team
					FROM matches
					WHERE start_time BETWEEN $1 AND $2`
	rows, err := db.Query(matchStatement, start, end)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		singleM := new(Match)
		err := rows.Scan(&singleM.Match_id, &singleM.Match_seq_num,
			&singleM.Radiant_win, &singleM.Start_time, &singleM.Duration,
			&singleM.Radiant_team, &singleM.Dire_team)
		if err != nil {
			log.Fatal(err)
		}
		matchdata = append(matchdata, *singleM)

	}
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "application/json")
	js, err := json.Marshal(matchdata)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(js)
}
